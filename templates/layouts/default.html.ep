% # vim:set sw=4 ts=4 sts=4 ft=html.epl expandtab:
<!DOCTYPE html>
<html>
    <head>
        <title><%= l('Framadrop - Partagez des fichiers de façon confidentielle') %></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        %= stylesheet '/css/framadrop.css'
        %= javascript begin
            function confirmExit() {
                console.log(i18n.confirmExit);
                return i18n.confirmExit;
            }
        % end
    </head>
    <body>
        %= javascript 'https://framasoft.org/nav/nav.js'
        %= javascript '/js/lufi-common.js'
        <div class="container ombre">
            <header>
                <h1><a href="<%= url_for('/') %>"><h1><b class="frama">Frama</b><b class="services">drop</b></h1></a></h1>
                <p class="lead"><%= l('Partagez des fichiers de façon confidentielle') %></p>
                <hr class="trait" role="presentation" />
            </header>
            <main>
                <div class="row">
                    <div class="col-md-12">
                        <nav>
                            <ul class="nav nav-tabs">
                                <li<%== ' class="active"' if (current_route eq 'index') %>><a href="<%= url_for('/') %>"><i class="fa fa-fw fa-lg fa-send"></i> <%= l('Upload files') %></a></li>
                                <li<%== ' class="active"' if (current_route eq 'files') %>><a href="<%= url_for('/files') %>"><i class="fa fa-fw fa-lg fa-history"></i> <%= l('My files') %></a></li>
                                <!--<li<%== ' class="active"' if (current_route eq 'about') %>><a href="<%= url_for('/about') %>"><%= l('About') %></a></li>-->
                            </ul>
                        </nav>
                        <%= content %>
                    </div>
                </div>
                <hr role="presentation" />
                <div class="row">
                    <div class="col-md-4" id="tuto-faq">
                        <h2>Prise en main</h2>
                        <p class="text-center" role="presentation"><i class="glyphicon glyphicon-question-sign"></i></p>
                        <div id="aboutbox">
                            <p><%== l('<b class="violet">Frama</b><b class="vert">drop</b> est un service en ligne libre qui permet de partager des fichiers de manière confidentielle.') %></p>
                            <ol>
                                <li><%= l('Si besoin, définissez la durée de conservation en ligne.') %></li>
                                <li><%= l('Collez le fichier à transmettre.') %></li>
                                <li><%= l('Partagez ensuite avec vos correspondants le lien qui vous est donné.') %></li>
                            </ol>
                            <p><%= l('Vos fichiers sont chiffrés et stockés sur nos serveurs sans qu’il nous soit possible de les déchiffrer.') %></p>

                            <p><%== l('Pour vous aider dans l’utilisation du logiciel, voici <a href="https://framatube.org/media/tutoriel-framadrop" data-toggle="modal" onclick="$(\'#TutoVideo\').modal(\'show\'); return false;">un tutoriel vidéo</a> réalisé par <a href="http://arpinux.org/">arpinux</a>, artisan paysagiste de la distribution GNU/Linux pour débutant <a href="https://handylinux.org/">HandyLinux</a>.') %></p>

    <!-- modale vidéo -->
    <div class="modal fade" id="TutoVideo" tabindex="-1" role="dialog" aria-labelledby="TutoVideoLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span><span class="sr-only">Fermer</span></button>
                    <h1 id="TutoVideoLabel"><%= l('Tutoriel vidéo') %></h1>
                </div>
                <div class="modal-body">
                    <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/988l.jpg" height="340" width="570">
                        <source src="https://framatube.org/blip/framadrop.mp4" type="video/mp4">
                        <source src="https://framatube.org/blip/framadrop.webm" type="video/webm">
                    </video></p>
                    <p>-&gt; <%== l('La <a href="https://framatube.org/blip/framadrop.webm">vidéo au format webm</a>') %></p>
               </div>
               <div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal"><%= l('Fermer') %></a></div>
           </div>
        </div>
    </div>
    <!-- /modale vidéo -->

                        </div>
                    </div>

                    <div class="col-md-4" id="le-logiciel">
                        <h2><%= l('Le logiciel') %></h2>
                        <p class="text-center" role="presentation"><i class="glyphicon glyphicon-cloud"></i></p>
                        <p><%== l('<b class="violet">Frama</b><b class="vert">drop</b> est une instance parmi d’autres du logiciel <a href="https://lufi.io">Lufi</a> développé par <a href="https://fiat-tux.fr/">Luc Didry</a>.') %></p>
                        <p><%== l('Lufi est <a href="https://www.gnu.org/licenses/agpl-3.0.html">sous licence libre <abbr title="GNU Affero General Public License">AGPL</abbr></a>.') %></p>
                        <p><%== l('Les sources de <b class="violet">Frama</b><b class="vert">drop</b> sont disponibles sur <a href="https://git.framasoft.org/framasoft/framadrop">notre forge logicielle</a>') %></p>
                    </div>

                    <div class="col-md-4" id="jardin">
                        <h2><%= l('Cultivez votre jardin') %></h2>
                        <p class="text-center" role="presentation"><i class="glyphicon glyphicon-tree-deciduous"></i></p>
                        <p><%== l('Pour participer au développement du logiciel, proposer des améliorations ou simplement le télécharger, rendez-vous sur <a href="https://git.framasoft.org/luc/lufi">le site de développement</a>.') %></p>
                        <p><%= l('Si vous souhaitez installer ce logiciel pour votre propre usage et ainsi gagner en autonomie, nous vous aidons sur :') %></p>
                        <p class="text-center"><a href="http://framacloud.org/cultiver-son-jardin/installation-de-lufi/" class="btn btn-success"><i class="glyphicon glyphicon-tree-deciduous"></i> framacloud.org</a></p>
                    </div>
                </div>
            </main>
        </div>
    </body>
</html>
